package com.telstra.codechallenge.github.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.telstra.codechallenge.github.api.exception.ServiceException;
import com.telstra.codechallenge.github.api.exception.ServiceUnavilableException;
import com.telstra.codechallenge.github.api.response.ErrorMapping;


@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler  {
 
	@ExceptionHandler(ServiceException.class)
	public ResponseEntity<ErrorMapping> exceptionHandler(ServiceException ex) {
		ErrorMapping error = new ErrorMapping();
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		error.setErrorMessage(ex.getMessage());
		System.out.println(error.toString());
		return new ResponseEntity<ErrorMapping>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ServiceUnavilableException.class)
	public ResponseEntity<ErrorMapping> exceptionHandler(ServiceUnavilableException ex) {
		ErrorMapping error = new ErrorMapping();
		error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		error.setErrorMessage(ex.getMessage());
		System.out.println(error.toString());
		return new ResponseEntity<ErrorMapping>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}