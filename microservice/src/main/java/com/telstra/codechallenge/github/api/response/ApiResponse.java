package com.telstra.codechallenge.github.api.response;

import java.util.List;

public class ApiResponse {

	List<Items> items;

	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}

}

