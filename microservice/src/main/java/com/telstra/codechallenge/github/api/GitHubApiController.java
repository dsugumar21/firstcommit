package com.telstra.codechallenge.github.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import java.util.List;
import com.telstra.codechallenge.github.api.response.ItemsHottestRepo;
import com.telstra.codechallenge.github.api.response.ItemsZeroFollowers;

import org.springframework.http.HttpStatus;

import com.telstra.codechallenge.github.api.exception.ServiceException;
import com.telstra.codechallenge.github.api.exception.ServiceUnavilableException;

@RestController
public class GitHubApiController {

	@Autowired
	private GitHubApiService gitHubApiService;

	@RequestMapping(path = "/zerofollowers", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<ItemsZeroFollowers>> zeroFollowers(@RequestParam(required = false) String parameter) throws ServiceException, ServiceUnavilableException {		
		List<ItemsZeroFollowers> response = null;

		if (gitHubApiService.validateInputInteger(parameter)) {
			try {
				response= gitHubApiService.getZeroFollower(parameter);
			}
			catch(Exception ex) {
				throw new ServiceUnavilableException("Internal Server Error, " + ex.getMessage());
			}
		}else {
			throw new ServiceException("Bad Input Parameter, parameter: " + parameter);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(path = "/hottestrepo", method = RequestMethod.GET,  produces = "application/json")
	public ResponseEntity<List<ItemsHottestRepo>> hottestRepo(@RequestParam(required = false) String parameter) throws ServiceException, ServiceUnavilableException {
		List<ItemsHottestRepo> response = null;

		if (gitHubApiService.validateInputInteger(parameter)) {
			try {
				response= gitHubApiService.gethottestRepo(parameter);
			}catch(Exception ex) {
				throw new ServiceUnavilableException("Internal Server Error, " + ex.getMessage());
			}
		}else {
			throw new ServiceException("Bad Input Parameter, parameter: " + parameter);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
