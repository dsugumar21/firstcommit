package com.telstra.codechallenge.github.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telstra.codechallenge.github.api.response.ApiResponse;
import com.telstra.codechallenge.github.api.response.ErrorMapping;
import com.telstra.codechallenge.github.api.response.ItemsHottestRepo;
import com.telstra.codechallenge.github.api.response.ItemsZeroFollowers;

@Service
public class GitHubApiService {

	public List<ItemsZeroFollowers> getZeroFollower(String param) throws MalformedURLException {

		List<ItemsZeroFollowers> list = new ArrayList<ItemsZeroFollowers>();
		String url= "https://api.github.com/search/users";
		Map<String, String> parameters = new HashMap<>();
		parameters.put("q", "followers:0");
		parameters.put("sort", "joined");
		parameters.put("order", "asc");

		ApiResponse apiResponse = connectGitUrl(url, parameters);
		if(validateInputInteger(param)) {
			for(int i = 0 ;i< Integer.parseInt(param); i++) {
				ItemsZeroFollowers items = new ItemsZeroFollowers();

				items.setLogin(apiResponse.getItems().get(i).getLogin());
				items.setId(apiResponse.getItems().get(i).getId());
				items.setHtml_url(apiResponse.getItems().get(i).getHtml_url());
				list.add(items);
			}
		} 

		return list;
	}

	public ApiResponse connectGitUrl(String url,Map<String, String> parameters) throws MalformedURLException{
		String responseBody = null;
		ApiResponse apiResponse = null;
		try {
			URLConnection  con = new URL(url + "?" + getParamsString(parameters)).openConnection();
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/vnd.github.preview");

			InputStream response = con.getInputStream();
			try (Scanner scanner = new Scanner(response)) {
				responseBody = scanner.useDelimiter("\\A").next();
			}

			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			apiResponse = objectMapper.readValue(responseBody,ApiResponse.class);

		}catch(IOException ex) {

		}
		return apiResponse;
	}

	public String getParamsString(Map<String, String> params) throws UnsupportedEncodingException{
		StringBuilder result = new StringBuilder();

		for (Map.Entry<String, String> entry : params.entrySet()) {
			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
			result.append("&");
		}

		String resultString = result.toString();
		return resultString.length() > 0 ? resultString.substring(0, resultString.length() - 1) : resultString;
	}

	public boolean validateInputInteger(String param) {

		if (null != param) {
			try {
				Integer.parseInt(param);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}else if (null == param)
			return false;
		return true;	
	}

	public List<ItemsHottestRepo> gethottestRepo(String param) throws MalformedURLException{

		List<ItemsHottestRepo> list = new ArrayList<ItemsHottestRepo>();

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			Date todate1 = cal.getTime();    
			String fromdate = dateFormat.format(todate1);

			String url= "https://api.github.com/search/repositories";
			Map<String, String> parameters = new HashMap<>();
			parameters.put("q", "created:>" + fromdate);
			parameters.put("sort", "stars");
			parameters.put("order", "desc");

			ApiResponse apiResponse = connectGitUrl(url, parameters);
			if(validateInputInteger(param)) {
				for(int i = 0 ;i< Integer.parseInt(param); i++) {

					ItemsHottestRepo items = new ItemsHottestRepo();

					items.setDescription(apiResponse.getItems().get(i).getDescription());
					items.setWatchers_count(apiResponse.getItems().get(i).getWatchers_count());
					items.setLanguage(apiResponse.getItems().get(i).getLanguage());
					items.setName(apiResponse.getItems().get(i).getName());
					items.setHtml_url(apiResponse.getItems().get(i).getHtml_url());

					list.add(items);
				}
			}

		return list;
	}
}
