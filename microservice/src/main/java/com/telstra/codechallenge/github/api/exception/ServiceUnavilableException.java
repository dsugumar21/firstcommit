package com.telstra.codechallenge.github.api.exception;

public class ServiceUnavilableException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public ServiceUnavilableException(String errorMessage)
	{
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	
	public ServiceUnavilableException()
	{
		super();
	}
}
