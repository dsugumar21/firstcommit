# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As a developer i want to test the zerofollowers uri

  Scenario: Is the hello uri available and functioning
    Given url microserviceUrl
    And path '/zerofollowers'
    And param parameter = 3
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response contains 
    """
    [{ id : '#string',
      login : '#string',
      html_url : '#string'
    },{ id : '#string',
      login : '#string',
      html_url : '#string'
    },{ id : '#string',
      login : '#string',
      html_url : '#string'
    }]
    """
    
    Scenario: Invalid Param in the zerofollowers
    Given url microserviceUrl
    And path '/zerofollowers'
    And param parameter = 1.0
    When method GET
    Then status 400
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == 
    """
    {
    "errorCode": '#number',
    "errorMessage": '#string'
    }
    """
    
    Scenario: No Param in the zerofollowers
    Given url microserviceUrl
    And path '/zerofollowers'
    When method GET
    Then status 400
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == 
    """
    {
    "errorCode": '#number',
    "errorMessage": '#string'
    }
    """
    


