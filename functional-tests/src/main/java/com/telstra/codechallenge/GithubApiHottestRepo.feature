# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As a developer i want to test the HottestRepo uri

  Scenario: Is the hello uri available and functioning
    Given url microserviceUrl
    And path '/hottestrepo'
    And param parameter = 3
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response contains 
    """
    [{ language : '#string',
       watchers_count : '#string',
       html_url : '#string',
       description : '#string',
       name : '#string'
    },{ language : '#string',
       watchers_count : '#string',
       html_url : '#string',
       description : '#string',
       name : '#string'
    },{ language : '#string',
       watchers_count : '#string',
       html_url : '#string',
       description : '#string',
       name : '#string'
    }]
    """
    
    Scenario: Invalid Param in the hottestrepo
    Given url microserviceUrl
    And path '/hottestrepo'
    And param parameter = 1.0
    When method GET
    Then status 400
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == 
    """
    {
    "errorCode": '#number',
    "errorMessage": '#string'
    }
    """
    
    Scenario: No Param in the hottestrepo
    Given url microserviceUrl
    And path '/hottestrepo'
    When method GET
    Then status 400
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    And match response == 
    """
    {
    "errorCode": '#number',
    "errorMessage": '#string'
    }
    """
    


